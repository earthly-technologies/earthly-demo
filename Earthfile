VERSION 0.6

FROM node:16.13.0-alpine3.14
WORKDIR /app

# Building only dependencies in this target takes advantage of caching
# based on only the package.json changes. This means we won't
# re-run the deps target when only the source code has changed.
deps:
    COPY package*.json . # package.json, package-lock.json
    RUN npm install

# The +code target depends on the +deps target, allowing us to cache the `npm install`.
# It brings in the rest of the .js files in the project needed to run the app.
code:
    FROM +deps
    COPY . .

# The +build target pulls together our code and dependencies from the previous
# targets and builds them into a docker container, ready to listen on port 3000.
build:
    FROM +code
    ARG EARTHLY_GIT_SHORT_HASH
    EXPOSE 3000
    ENTRYPOINT ["npm", "start"]
    SAVE IMAGE --push earthlydemo/earthly-demo:$EARTHLY_GIT_SHORT_HASH
