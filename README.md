# Earthly GitLab Demo

A simple demo of how to build a project using Earthly on GitLab.

The project contains a basic REST API in Node.Js with an Earthfile that builds a container.

See [`.gitlab-ci.yml`](https://gitlab.com/earthly-technologies/earthly-demo/-/blob/main/.gitlab-ci.yml) on how to trigger the Earthly build using GitLab's CI/CD.
